const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const Dotenv = require('dotenv-webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        inline: true,
        port: 3000,
        contentBase: './public/'
    },
    module: {
        rules: [
           {
              test: /\.jsx?$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
           },
           {
            test: /\.html$/,
            use: [
              {
                loader: 'html-loader'
              }
            ]
          },
          {
            test: /\.css$/i,
            use: ['style-loader','css-loader'],
          },
        ]
     },
     plugins:[
        new HtmlWebpackPlugin({
           template: './public/index.html',
           filename: './index.html'
        }),
        new MiniCssExtractPlugin({
         filename: "css/[name].css",
         chunkFilename: "css/[id].css"
       }),
        new Dotenv()
     ]
};
