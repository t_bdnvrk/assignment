# Description

## Intro

Application was made along with the rules provided in `ASSIGNMENT.md`. It presents list of articles based on selected dataset (provided by api). Articles are always sorted and there has to be at least one dataset selected (I made that required)

Application was written in `React` with usage of `styled-components` - this choice maybe was not the best fit just to solve the given problem, but since applying for React based role I decided to use this library. Syled components was my personal just because I like them.


## Setup

Hi there, to run this application you need:

First you need to instal depencencies with `yarn install` or `npm install` 

Setup `.env` file providing API host with key `API_HOSTNAME`

If you want to run application localy, first you need to run server with command:
> `$ node server.js`
> 
Then you can run application with:
> `$ yarn startDev` or `$ npm run startDev`

For this project I have been using `yarn`

To run tests run in console:
> `yarn test` or `yarn test:watch`

To build application run in console:
> `yarn build`

NOTE: While running build please provide env variable defining API host name in `package.json`
>`"build": "API_HOSTNAME=your_host_name webpack"`


## To improve
I could list many things that I would imporove in the final product. Some of the solutions were made only just for the sake of this task and weren't universal. Examples:

- I could use moment.js for date parsing. That was one of the problems that I've encountered while doing this task. Date format was not standard an I had to parse it manualy
- I could handle errors better, altough there is one possible error related to fetching data, in normal project I would abstract error handling logic to be universal across components
- I could have used plain `js` rather than `React` just for the sake of providing fastest solution (budnle size would be smaller)
- I could write plain `css` rather than `styled-components`. In real life project I wouldn't force usage of this library, I would rather disscus what solution would suit project better and what are preferences of other
- Project structure could be also changed. My personal choice would be little bit flatened atomic design without some component types (depends on project)
- Testing - For UI tests I would use something like `cypress`
- I would insist on changing api to accept category arguments rather than using 2 separate endpoints to fetch the same type of data 😁
- better setup with dotenv - current setup is very basic and it's not working with webpack build. 
- in normal project I would use linter - didn't need that here
- I would exclude `.env` file from the source, here it's included to enable seamles project run

# Schibsted frontend task app

This is a very basic npm project with default webpack setup.

You should use this project as a base for your solution.
Feel free to modify/extend this with whatever you need.

In case you need some assistance take a look at official webpack docs:
 - [Guides](https://webpack.js.org/guides/) 
 - [Concepts](https://webpack.js.org/concepts/)

Eventually this application will request live data from a real API.

You can find the full description of your assignment in [ASSIGNMENT.md](ASSIGNMENT.md)

## API Documentation
Api server can be found in `server.js` file. You should not modify this file, only use it.

To run the server do:
> `$ node server.js`

Server will start listening on port `6010`.

The server has 2 endpoints:

`/articles/sports` - returns a list of articles from `sport` category

`/articles/fashion` - returns a list of articles from `fashion` category

Be aware of backend errors!
