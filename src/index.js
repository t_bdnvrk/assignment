import React from 'react';
import ReactDOM from 'react-dom';
import ApplicationRoot from './ApplicationRoot';
import './styles/reset.css';
import './styles/global.css';

ReactDOM.render(<ApplicationRoot />, document.getElementById('root'));