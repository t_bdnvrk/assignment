import React, { useState } from 'react';
import Dashboard from '../components/Dashboard';
import {
  ApplicationContainer,
  DataSetPicker,
  StyledForm,
  StyledLabel,
  SmallHeader,
} from './components';


const ApplicationRoot = () => {
  const [selectedDatasets, setSelectedDatasets] = useState(['fashion']);

  function toggleDataSet(dataset) {
    const datasetIndex = selectedDatasets.indexOf(dataset);
    if(datasetIndex === -1) {
      setSelectedDatasets([...selectedDatasets, dataset]);
    } else if(selectedDatasets.length !== 1) {
      setSelectedDatasets(selectedDatasets.filter((dset) => dset !== dataset));
    }
  }

  return (
    <ApplicationContainer>
      <DataSetPicker>
        <SmallHeader>Data sources:</SmallHeader>
        <StyledForm>
          <StyledLabel htmlFor="fashion" >
            <input
              type="checkbox"
              name="fashion"
              id="fashion"
              checked={selectedDatasets.includes('fashion')}
              onChange={() => toggleDataSet('fashion')}
              data-testid="fashion-checkbox"
            />
            {'Fashion'} 
          </StyledLabel>
          <StyledLabel htmlFor="sports">
            <input
              type="checkbox"
              name="sports"
              id="sports"
              checked={selectedDatasets.includes('sports')} 
              onChange={() => toggleDataSet('sports')}
              data-testid="sports-checkbox"
            />
            {'Sports'}
          </StyledLabel>
        </StyledForm>
      </DataSetPicker>
      <Dashboard categories={selectedDatasets}/>
    </ApplicationContainer>
  );
};

export default ApplicationRoot;
