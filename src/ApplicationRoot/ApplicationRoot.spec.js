import React from 'react';
import { render, screen, act, fireEvent } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import ApplicationRoot from './index';

const mockData = [
  { id: 1, date: '1. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 1', preamble: 'test preamble 1' },
  { id: 2, date: '2. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 2', preamble: 'test preamble 2' },
  { id: 3, date: '3. mai 2017', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 3', preamble: 'test preamble 3' },
  { id: 4, date: '4. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 4', preamble: 'test preamble 4' },
  { id: 5, date: '5. februar 2018', image: 'https://placeimg.com/280/180/nature', category: 'fashion', title: 'test title 5', preamble: 'test preamble 5' },
  { id: 6, date: '6. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'fashion', title: 'test title 6', preamble: 'test preamble 6' },
  { id: 7, date: '7. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'fashion', title: 'test title 7', preamble: 'test preamble 7' },
];

const baseUrl = ''
const filterCategory = (category) => (mockData.filter((article) => article.category === category));
const parseUrlToCategory = (url) => (url.includes('fashion') ? 'fashion' : 'sport');

const mockFetch = (url) => Promise.resolve({
  status: 200,
  json: () => ({ articles: filterCategory(parseUrlToCategory(url)) }),
})

const mockJson = Promise.resolve({
  mockData
});

global.fetch = jest.fn().mockImplementation((url) => mockFetch(url));


describe('<ApplicationRoot>', () => {


  test('component renders', async () => {
    await act(async () => {
      render(<ApplicationRoot />);
    });
  });

  test('components renders correct list of articles for category changes', async () => {
    await act(async () => {
      render(<ApplicationRoot />);
      const checkbox = screen.getByTestId('sports-checkbox')
      fireEvent.click(checkbox)
    });

    expect(screen.getAllByTestId('article-container').length).toBe(7);
  });

})