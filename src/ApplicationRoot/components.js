import styled from 'styled-components';

const ApplicationContainer = styled.div`
  display:flex;
  max-width: 1280px;
  margin: 0 auto;

  @media (max-width: 1280px) {
    flex-direction: column;
    padding: 0px 20px;
  }
`;

const DataSetPicker = styled.div`
  display: flex;
  flex-direction: column;
  margin: 20px auto;
  @media (max-width: 1280px) {
    margin: 20px;
  }
`;

const StyledForm = styled.form`
  display: flex;
  flex-direction: column;

  @media (max-width: 1280px) {
    flex-direction: row;

    label {
      margin-right: 20px;
    }
  }
`;

const StyledLabel = styled.label`
  margin-bottom: 10px;

  input {
    margin-right: 10px;
  }
`;

const SmallHeader = styled.h3`
  font-size: 18px;
  font-weight: 600;
  margin-bottom: 15px;
`;

export {
  ApplicationContainer,
  DataSetPicker,
  StyledForm,
  StyledLabel,
  SmallHeader,
}