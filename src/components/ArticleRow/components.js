import styled from 'styled-components';

const ArticleContainer = styled.li`
  width: 100%;
  display: flex;
  margin-bottom: 20px;
  box-shadow: 1px 1px 0px rgba(0, 0, 0, 0.2);

  @media (max-width: 600px) {
    flex-direction: column
  }
`;

const ArticleInfo = styled.div`
  display: flex;
  flex: 1;
  flex-direction: column;
  padding: 20px;
`;

const ImageContainer = styled.div`
  width: 280px;
  @media (max-width: 600px) {
    width: 100%;
  }
`;

const TopRow = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Image = styled.img`
  max-width:100%;
  max-height:100%;
  display: block;

  @media (max-width: 600px) {
    margin: 0 auto;
  }
`;

const ArticleTitle = styled.h2`
  flex: 1;
  font-size: 23px;
  font-weight: 600;
  margin-bottom: 30px;
`;

const StyledPlaceholder = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  color: white;
  background-color: #ccc;

  @media (max-width: 600px) {
    display: none;
  }
`;

const Date = styled.span`
  font-weight: 300;
  display: block;
`;

const Preamble = styled.p`
  @media (max-width: 768px) {
    display: none;
  }
`;

export {
  ArticleContainer,
  ArticleInfo,
  ImageContainer,
  TopRow,
  Image,
  ArticleTitle,
  StyledPlaceholder,
  Date,
  Preamble
};
