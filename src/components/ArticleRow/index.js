import React from 'react'
import {
  ArticleContainer,
  ArticleInfo,
  ImageContainer,
  TopRow,
  Image,
  ArticleTitle,
  StyledPlaceholder,
  Date,
  Preamble
} from './components';


const isValidUrl = (url) => {
  const regexp = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/g;
  if(url && url !== null) {
    return regexp.test(url);
  }
  return false;
}

const ArticleRow = ({
  image,
  title,
  preamble,
  date,
}) => (
  <ArticleContainer data-testid="article-container">
    {isValidUrl(image) ? (
      <ImageContainer>
        <Image data-testid="image" src={image} alt="article image" />
      </ImageContainer>
    ) : (
      <ImageContainer>
        <StyledPlaceholder>
           Placeholder
        </StyledPlaceholder>  
      </ImageContainer>
    )}
    <ArticleInfo>
      <TopRow>
        <ArticleTitle data-testid="title">{title}</ArticleTitle>
        <Date data-testid="date">{date}</Date>
      </TopRow>
      {preamble && preamble !== null && (
        <Preamble data-testid="preamble">{preamble}</Preamble>
      )}
    </ArticleInfo>
  </ArticleContainer>
);

export default ArticleRow;
