import React from 'react';
import { render, screen, act } from '@testing-library/react';
import '@testing-library/jest-dom/extend-expect'
import ArticleRow from './index';

const props = (image, preamble) => ({
    id: 789702,
    date: '2. februar 2019', 
    title: 'test title', 
    category: 'sport', 
    image, //'https://placeimg.com/280/180/nature', 
    preamble,
})

describe('<ArticleRow> ', () => {
  const { getByTestId, queryByTestId } = render(<ArticleRow {...props('https://placeimg.com/280/180/nature', 'test preamble')}/>);

  test('given complete props component renders displaying every element', () => {
    expect(getByTestId('image')).toBeTruthy();
    expect(getByTestId('title').textContent).toBe('test title');
    expect(getByTestId('preamble').textContent).toBe('test preamble');
    expect(getByTestId('date').textContent).toBe('2. februar 2019');
  });

  test('given props some props preamble and image are not displaying', () => {
    render(<ArticleRow {...props()}/>)

    expect(queryByTestId('image')).toBeNull();
    expect(queryByTestId('title').textContent).toBe('test title');
    expect(queryByTestId('preamble')).toBeNull();
    expect(queryByTestId('date').textContent).toBe('2. februar 2019');
  });
});
