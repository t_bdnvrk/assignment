import styled from 'styled-components';

const Container = styled.div`
  max-width: 1024px;
  width: 100%;
  display: flex;
  flex-direction: column;
  margin: 0 auto;
`;

const ArticlesSection = styled.ul`
  display: flex;
  flex-direction: column;
`;

const SortPanel = styled.div`
  display: flex;
  justify-content: flex-end;
  margin: 20px 0px;
`;

const SortButton = styled.button`
  position: relative;
  padding: 10px 35px 10px 15px;
  border: 1px solid black;
  background: none;
  font-weight: 300;
  color: black;
  &:hover {
    box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.5)
  }
  &::after {
    position: absolute;
    content: ' ';
    width: 10px;
    height: 10px;
    right: 12px;
    top: 53%;
    border-bottom: 1px solid black;
    border-left: 1px solid black;
    transform: translateY(-50%) rotate(${({ direction }) => direction === 'desc' ? '-45deg' : '135deg'});
  }

  &:disabled {
    border-color: #ccc;
    color: #ccc;

    cursor: auto;

    &::after {
      border-color: #ccc;
    }

    &:hover {
      box-shadow: none;
    }
  }
`;

const ErrorMessage = styled.h2`
  font-size: 23px;
  text-align: center;
  margin-top: 10%;
  height: 65px;
  padding-top: 65px;
  position: relative;

  &:before {
    position: absolute;
    content: '!';
    font-size: 30px;
    padding: 10px 20px;
    color: #db0011;
    border: 1px solid #db0011;
    border-radius: 100%;
    left: 50%;
    top: 0px;
    transform: (translateX(-50px))
  }
`

export {
  Container,
  ArticlesSection,
  SortPanel,
  SortButton,
  ErrorMessage,
};
