import React from 'react';
import { render, screen, act, rerender, cleanup } from '@testing-library/react';


import '@testing-library/jest-dom/extend-expect'

import Dashboard from './index';

const mockData = {
  articles: [
    { id: 1, date: '1. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 1', preamble: 'test preamble 1' },
    { id: 2, date: '2. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 2', preamble: 'test preamble 2' },
    { id: 3, date: '3. mai 2017', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 3', preamble: 'test preamble 3' },
    { id: 4, date: '4. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'sport', title: 'test title 4', preamble: 'test preamble 4' },
    { id: 6, date: '5. februar 2018', image: 'https://placeimg.com/280/180/nature', category: 'fashion', title: 'test title 5', preamble: 'test preamble 5' },
    { id: 7, date: '6. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'fashion', title: 'test title 6', preamble: 'test preamble 6' },
    { id: 8, date: '7. februar 2019', image: 'https://placeimg.com/280/180/nature', category: 'fashion', title: 'test title 7', preamble: 'test preamble 7' },
  ]
};


afterEach(cleanup);

describe('<Dashboard/>:', () => {

  const mockFetch = Promise.resolve({
    status: 200,
    json: () => mockData
  })

  const mockJson = Promise.resolve({
    mockData
  });
  
  global.fetch = jest.fn().mockImplementation(() => mockFetch);

  test('Component fetches for article data', async () => {

    await act(async () => {
      render(<Dashboard categories={['fashion']} />);
    });

    expect(global.fetch).toHaveBeenCalledWith('http://localhost:6010/articles/fashion', { method: 'GET' });
  });

  test('Component fetches for sports data', async () => {
    await act(async () => {
      render(<Dashboard categories={['sports']} />);
    });

    expect(global.fetch).toHaveBeenCalledWith('http://localhost:6010/articles/sports', { method: 'GET' });
  });
});