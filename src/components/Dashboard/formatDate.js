function translateMonthToNumber(month) {
  switch(month) {
    case 'januar': return '01';
    case 'februar': return '02';
    case 'mars': return '03';
    case 'april': return '04';
    case 'mai': return '05';
    case 'juni': return '06';
    case 'juli': return '07';
    case 'august': return '08';
    case 'september': return '09';
    case 'oktober': return '10';
    case 'november': return '11';
    case 'desember': return '12';
    default: return null;
  }
}

export function formatDate (date) {
  const newDate = date.replace('.', '')
  const [day, month, year] = date.replace('.', '').split(' ');
  return new Date(year, translateMonthToNumber(month), day);
} 