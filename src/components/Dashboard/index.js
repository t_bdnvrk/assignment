import React, { useReducer, useEffect } from 'react';

import ArticleRow from '../ArticleRow';
import {
  Container,
  ArticlesSection,
  SortPanel,
  SortButton,
  ErrorMessage,
} from './components';
import { formatDate } from './formatDate';

const defaultState = {
  loading: true,
  data: [],
  sortDirection: 'asc',
  loadingFailed: false,
}

const reducer = (state, action) => {
  if (action.type === 'SET_STATE') {
    return {...state, ...action.payload };
  }
  return state;
};

const Dashboard = ({ categories = ['fashion', 'sports']}) => {
  const [state, dispatch] = useReducer(reducer, defaultState);
  const setState = (payload) => dispatch({ type: 'SET_STATE', payload });
  const {
    loading,
    data,
    sortDirection,
    loadingFailed,
  } = state;

  useEffect(() => {
    const requestData = () => {

      const listToFetch = categories.map((category) => {
        return fetch(`${process.env.API_HOSTNAME}/articles/${category}`, { method: 'GET'});
      });

      Promise.all(listToFetch)
        .then((responses) => {
          return responses.map((response) => {
            if(response.status === 200) {
               return response.json();
            } else {
              throw "Error"
            }
          });
          
        })
        .then((fetchedData) => {
          Promise.all(fetchedData)
            .then((articlesData) => {
              const combinedArticles = articlesData.reduce((arr, articles) => {
                return arr.concat(articles.articles);
              }, []);
              setState({ data: combinedArticles, loading: false, loadingFailed: false });
            })
        })
        .catch((error) => {

          setState({ loading: false, loadingFailed: true })
          console.error(error)
        });
    };

    requestData();
  }, [categories]);

  function sortArticles() {
    const dataCopy = [...data];
    if (sortDirection === 'asc') {
      return dataCopy.sort((a, b) => {
        const dateA = formatDate(a.date).getTime();
        const dateB = formatDate(b.date).getTime();

        return dateA - dateB;
      });
    } else {
      return dataCopy.sort((a, b) => {
        const dateA = formatDate(a.date).getTime();
        const dateB = formatDate(b.date).getTime();

        return dateB - dateA;
      });
    }
  }

  function changeSortDirection() {
    setState({ sortDirection: sortDirection === 'asc' ? 'desc' : 'asc'});
  }
  return (
    <Container>
      <SortPanel>
        <SortButton
          direction={sortDirection}
          onClick={() => changeSortDirection()}
          disabled={loadingFailed}
        >
          Sort by date
        </SortButton>
      </SortPanel>
      {!loadingFailed ? (
        <ArticlesSection>
          {data.length > 0 && !loading && sortArticles(data).map((article) => (
            <ArticleRow key={article.id} {...article} />
          ))}
        </ArticlesSection>
      ) : (
        <ErrorMessage>
          Something went wrong, please try selecing other data sources or refresh the page.
          If issue continues to appear contact our support sample.mail@mail.com 
        </ErrorMessage>
      )}
    </Container>
  );
}

export default Dashboard;